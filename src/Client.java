import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client {

	private static final int PLAYER_1 = 0, PLAYER_2 = 1;

	public static void main(String[] args) {

		//test();
		 Client client = new Client();
		 client.start();
	}

	private static void test() {
		Board board = new Board();
		int player = PLAYER_1;
		while (true) {
			String move = BoardEval.getInstance().getNextMove(board, player);
			board.move(move);
			board.printGrid();
			player ^= 1;

			if (board.playerWon(player) || board.playerWon(player ^ 1))
				break;
		}
	}

	Socket socket;
	BufferedInputStream input;
	BufferedOutputStream output;
	BufferedReader console;
	Board board;
	int MY_PLAYER = -1;

	public void start() {

		try {
			socket = new Socket("localhost", 8888);
			input = new BufferedInputStream(socket.getInputStream());
			output = new BufferedOutputStream(socket.getOutputStream());
			console = new BufferedReader(new InputStreamReader(System.in));
			board = new Board();

			if (socket.isConnected()) {

				boolean running = true;
				while (running) {
					running = readSocketAnswer((char) input.read());
				}
			} else {
				socket.close();
			}

		} catch (IOException e) {
			System.out.println(e);
		}
	}

	private boolean readSocketAnswer(char cmd) throws IOException {

		// D�but de la partie en joueur blanc
		if (cmd == '1') {
			MY_PLAYER = PLAYER_1;

			byte[] aBuffer = new byte[1024];

			int size = input.available();
			input.read(aBuffer, 0, size);

			String s = new String(aBuffer).trim();

			System.out
					.println("Nouvelle partie! Vous jouer blanc, entrez votre premier coup : ");

			board.printGrid();

			String move = BoardEval.getInstance().getNextMove(board, MY_PLAYER);
			// System.out.println("Mon coup: " + move);
			board.move(move);

			output.write(move.getBytes(), 0, move.length());
			output.flush();
			return true;
		}

		// D�but de la partie en joueur Noir
		else if (cmd == '2') {
			MY_PLAYER = PLAYER_2;
			System.out
					.println("Nouvelle partie! Vous jouer noir, attendez le coup des blancs");

			byte[] aBuffer = new byte[1024];
			int size = input.available();
			input.read(aBuffer, 0, size);

			String s = new String(aBuffer).trim();
			return true;
		}

		// Le serveur demande le prochain coup
		// Le message contient aussi le dernier coup jou�.
		else if (cmd == '3') {
			byte[] aBuffer = new byte[16];

			int size = input.available();
			input.read(aBuffer, 0, size);

			String s = new String(aBuffer);
			// System.out.println("Coup de l'autre joueur : " + s);
			board.move(s);

			String move = BoardEval.getInstance().getNextMove(board, MY_PLAYER);
			// System.out.println("Mon coup : " + move);

			board.move(move);
			// board.printGrid();

			output.write(move.getBytes(), 0, move.length());
			output.flush();
			return true;

		}
		// Le dernier coup est invalide
		else if (cmd == '4') {
			System.out.println("Coup invalide, entrez un nouveau coup : \n");

			String move = BoardEval.getInstance().getNextMove(board, MY_PLAYER);
			output.write(move.getBytes(), 0, move.length());
			output.flush();
			return true;
		}
		return false;
	}
}
