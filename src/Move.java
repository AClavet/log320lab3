import java.util.Iterator;
import java.util.Vector;

/**
 * 
 * @author aleks
 * 
 */
public final class Move implements Comparable<Move> {

	public final int from;
	public final int to;
	public final Board board;
	public int type = 0;
	public int score = 0;
	public int depth = 0;
	public int toWin = 0;
	public int toLose = 0;

	public Move(int from, int to, Board board, int movePriority, int player) {
		this.from = from;
		this.to = to;
		this.board = board;
		this.type = movePriority;
	}

	public Move(Move m) {
		this.from = m.from;
		this.to = m.to;
		this.board = m.board;
		this.type = m.type;
		this.depth = m.depth;
		this.score = m.score;
	}

	@Override
	public int compareTo(Move o) {
		if (this.type < o.type)
			return -1;
		else if (this.type > o.type)
			return 1;
		else
			return 0;
	}

	public boolean isOk(Vector<Move> moves) {
		Iterator it = moves.iterator();
		while (it.hasNext()) {
			Move o = (Move) it.next();
			if (o.from == this.from && o.to == this.to) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Move priorities
	 */
	public static int LOST = 6;
	public static int MOVE_PUSHEE = 5;
	public static int MOVE_PUSHER = 4;
	public static int ATTACK_PUSHEE = 3;
	public static int ATTACK_PUSHER = 2;
	public static int WIN = 1;
}
