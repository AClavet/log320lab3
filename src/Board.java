import java.util.List;
import java.util.Vector;

public final class Board {

	public static long centerMask;
	public static long p1cornerMask;
	public static long p2cornerMask;
	public static long center_squares_Mask;

	private long P1_PUSHERS, P2_PUSHERS, P1_PUSHEES, P2_PUSHEES;
	public int score = 0;

	public boolean p1_cantMove = false;
	public boolean p2_cantMove = false;

	public Board() {

		P1_PUSHERS = 255; // Set 8 first Bit to 1
		P1_PUSHEES = 255L << 8; // Set Bits 9 to 16 to 1

		P2_PUSHEES = 255L << 48; // Set Bits 49 to 56 to 1
		P2_PUSHERS = 255L << 56; // Set the last 8 Bits to 1

	}

	/**
	 * Create a Bitboard from an existing one.
	 * 
	 * @param board
	 */
	public Board(Board board) {
		this.P1_PUSHEES = board.P1_PUSHEES;
		this.P2_PUSHEES = board.P2_PUSHEES;
		this.P1_PUSHERS = board.P1_PUSHERS;
		this.P2_PUSHERS = board.P2_PUSHERS;
	}

	/**
	 * Get empty squares
	 * 
	 * @return
	 */
	private long getEmptySquares() {
		return ~getOccupiedSquares();
	}

	/**
	 * Get squares occupied by any player.
	 * 
	 * @return
	 */
	private long getOccupiedSquares() {
		return P1_PUSHERS | P2_PUSHERS | P1_PUSHEES | P2_PUSHEES;
	}

	/**
	 * Get squares occupied by player 1
	 * 
	 * @return
	 */
	private long getP1() {
		return P1_PUSHERS | P1_PUSHEES;
	}

	/**
	 * Get squares occupied by player 2
	 * 
	 * @return
	 */
	private long getP2() {
		return P2_PUSHERS | P2_PUSHEES;
	}

	/**
	 * Returns all available move for the specified player
	 * 
	 * @param player
	 *            0 = P1, 1 = P2
	 * @return
	 */
	public List<Move> getMoves(int player) {
		if ((player & 1) == 0) {
			return getP1Moves();
		} else {
			return getP2Moves();
		}
	}

	private Vector<Move> getP1Moves() {
		Vector<Move> moves = new Vector<Move>();
		int player = 0;
		long empty = getEmptySquares();
		long ennemy = getP2();

		// Pour chaque pusher
		for (int i = 0; i < 64; i++) {
			if (((P1_PUSHERS >> i) & 1L) == 0L)
				continue;

			// Move left ?

			int left = i + 7;
			if (Bit.isSet(empty, left) && isNextRow(i, left)) {
				Board board = new Board(this);

				board.P1_PUSHERS = Bit.clear(P1_PUSHERS, i);
				board.P1_PUSHERS = Bit.set(board.P1_PUSHERS, left);

				moves.add(new Move(i, left, board, Move.MOVE_PUSHER, player));
			}

			// Move forward ?
			if (Bit.isSet(empty, i + 8)) {
				Board board = new Board(this);

				board.P1_PUSHERS = Bit.clear(P1_PUSHERS, i);
				board.P1_PUSHERS = Bit.set(board.P1_PUSHERS, i + 8);

				moves.add(new Move(i, i + 8, board, Move.MOVE_PUSHER, player));
			}

			// Move right ?
			if (Bit.isSet(empty, i + 9) && isNextRow(i, i + 9)) {
				Board board = new Board(this);

				board.P1_PUSHERS = Bit.clear(P1_PUSHERS, i);
				board.P1_PUSHERS = Bit.set(board.P1_PUSHERS, i + 9);

				moves.add(new Move(i, i + 9, board, Move.MOVE_PUSHER, player));
			}

			// Attack left ?
			if (Bit.isSet(ennemy, left) && isNextRow(i, left)) {
				Board board = new Board(this);

				board.P1_PUSHERS = Bit.clear(P1_PUSHERS, i);
				board.P1_PUSHERS = Bit.set(board.P1_PUSHERS, left);

				int move = 0;
				// Attacked a pushee ?
				if (Bit.isSet(P2_PUSHEES, left)) {
					board.P2_PUSHEES = Bit.clear(P2_PUSHEES, left);
					move = Move.ATTACK_PUSHEE;
				}// Otherwise it was a pusher
				else {
					board.P2_PUSHERS = Bit.clear(P2_PUSHERS, left);
					move = Move.ATTACK_PUSHER;
				}
				moves.add(new Move(i, left, board, move, player));
			}

			// Attack right ?
			if (Bit.isSet(ennemy, i + 9) && isNextRow(i, i + 9)) {
				Board board = new Board(this);

				board.P1_PUSHERS = Bit.set(P1_PUSHERS, i + 9);
				board.P1_PUSHERS = Bit.clear(board.P1_PUSHERS, i);

				int move = 0;
				// Attacked a pushee ?
				if (Bit.isSet(P2_PUSHEES, i + 9)) {
					board.P2_PUSHEES = Bit.clear(P2_PUSHEES, i + 9);
					move = Move.ATTACK_PUSHEE;
				}// Otherwise it was a pusher
				else {
					board.P2_PUSHERS = Bit.clear(P2_PUSHERS, i + 9);
					move = Move.ATTACK_PUSHER;
				}
				moves.add(new Move(i, i + 9, board, move, player));
			}

			// Push left ?
			if (Bit.isSet(P1_PUSHEES, left) && Bit.isSet(empty, i + 14)
					&& isNextRow(i, left) && isSecondRow(i, i + 14)) {
				Board board = new Board(this);

				board.P1_PUSHEES = Bit.clear(P1_PUSHEES, left);
				board.P1_PUSHEES = Bit.set(board.P1_PUSHEES, i + 14);

				moves.add(new Move(left, i + 14, board, Move.MOVE_PUSHEE,
						player));
			}

			// Push forward ?
			if (Bit.isSet(P1_PUSHEES, i + 8) && Bit.isSet(empty, i + 16)) {
				Board board = new Board(this);

				board.P1_PUSHEES = Bit.clear(P1_PUSHEES, i + 8);
				board.P1_PUSHEES = Bit.set(board.P1_PUSHEES, i + 16);

				moves.add(new Move(i + 8, i + 16, board, Move.MOVE_PUSHEE,
						player));
			}

			// Push right ?
			if (Bit.isSet(P1_PUSHEES, i + 9) && Bit.isSet(empty, i + 18)
					&& isNextRow(i, i + 9) && isSecondRow(i, i + 18)) {
				Board board = new Board(this);

				board.P1_PUSHEES = Bit.clear(P1_PUSHEES, i + 9);
				board.P1_PUSHEES = Bit.set(board.P1_PUSHEES, i + 18);

				moves.add(new Move(i + 9, i + 18, board, Move.MOVE_PUSHEE,
						player));
			}

			// Push attack left ?
			if (Bit.isSet(P1_PUSHEES, left) && Bit.isSet(ennemy, i + 14)
					&& isNextRow(i, left) && isSecondRow(i, i + 14)) {
				Board board = new Board(this);

				board.P1_PUSHEES = Bit.set(P1_PUSHEES, i + 14);
				board.P1_PUSHEES = Bit.clear(board.P1_PUSHEES, left);

				int move = 0;
				// Attacked a pushee ?
				if (Bit.isSet(P2_PUSHEES, i + 14)) {
					board.P2_PUSHEES = Bit.toggle(P2_PUSHEES, i + 14);
					move = Move.ATTACK_PUSHEE;
				}// Otherwise it was a pusher
				else {
					board.P2_PUSHERS = Bit.toggle(P2_PUSHERS, i + 14);
					move = Move.ATTACK_PUSHER;
				}
				moves.add(new Move(left, i + 14, board, move, player));
			}

			// Push attack right ?
			if (Bit.isSet(P1_PUSHEES, i + 9) && Bit.isSet(ennemy, i + 18)
					&& isNextRow(i, i + 9) && isSecondRow(i, i + 18)) {
				Board board = new Board(this);

				board.P1_PUSHEES = Bit.clear(P1_PUSHEES, i + 9);
				board.P1_PUSHEES = Bit.set(board.P1_PUSHEES, i + 18);

				int move = 0;
				// Attacked a pushee ?
				if (Bit.isSet(P2_PUSHEES, i + 18)) {
					board.P2_PUSHEES = Bit.clear(P2_PUSHEES, i + 18);
					move = Move.ATTACK_PUSHEE;
				}// Otherwise it was a pusher
				else {
					board.P2_PUSHERS = Bit.clear(P2_PUSHERS, i + 18);
					move = Move.ATTACK_PUSHER;
				}
				moves.add(new Move(i + 9, i + 18, board, move, player));
			}
		}

		if (moves.size() == 0)
			p1_cantMove = true;
		return moves;
	}

	private Vector<Move> getP2Moves() {
		Vector<Move> moves = new Vector<Move>();
		int player = 1;

		long empty = getEmptySquares();
		long ennemy = getP1();

		for (int i = 0; i < 64; i++) {
			if (((P2_PUSHERS >> i) & 1L) == 0L)
				continue;

			// Move left ?
			int left = i - 7;
			if (Bit.isSet(empty, left) && isNextRow(i, left)) {
				Board board = new Board(this);

				board.P2_PUSHERS = Bit.clear(P2_PUSHERS, i);
				board.P2_PUSHERS = Bit.set(board.P2_PUSHERS, left);

				moves.add(new Move(i, left, board, Move.MOVE_PUSHER, player));
			}

			// Move forward ?
			int front = i - 8;
			if (Bit.isSet(empty, front)) {
				Board board = new Board(this);

				board.P2_PUSHERS = Bit.clear(P2_PUSHERS, i);
				board.P2_PUSHERS = Bit.set(board.P2_PUSHERS, front);

				moves.add(new Move(i, front, board, Move.MOVE_PUSHER, player));
			}

			// Move right ?
			int right = i - 9;
			if (Bit.isSet(empty, right) && isNextRow(i, right)) {
				Board board = new Board(this);

				board.P2_PUSHERS = Bit.clear(P2_PUSHERS, i);
				board.P2_PUSHERS = Bit.set(board.P2_PUSHERS, right);

				moves.add(new Move(i, right, board, Move.MOVE_PUSHER, player));
			}

			// Attack left ?
			if (Bit.isSet(ennemy, left) && isNextRow(i, left)) {
				Board board = new Board(this);

				board.P2_PUSHERS = Bit.clear(P2_PUSHERS, i);
				board.P2_PUSHERS = Bit.set(board.P2_PUSHERS, left);

				int move = 0;
				// Attacked a pushee ?
				if (Bit.isSet(P1_PUSHEES, left)) {
					board.P1_PUSHEES = Bit.clear(P1_PUSHEES, left);
					move = Move.ATTACK_PUSHEE;
				}// Otherwise it was a pusher
				else {
					board.P1_PUSHERS = Bit.clear(P1_PUSHERS, left);
					move = Move.ATTACK_PUSHER;
				}
				moves.add(new Move(i, left, board, move, player));
			}

			// Attack right ?
			if (Bit.isSet(ennemy, right) && isNextRow(i, right)) {
				Board board = new Board(this);

				board.P2_PUSHERS = Bit.set(P2_PUSHERS, right);
				board.P2_PUSHERS = Bit.clear(board.P2_PUSHERS, i);

				int move = 0;
				// Attacked a pushee ?
				if (Bit.isSet(P1_PUSHEES, right)) {
					board.P1_PUSHEES = Bit.clear(P1_PUSHEES, right);
					move = Move.ATTACK_PUSHEE;
				}// Otherwise it was a pusher
				else {
					board.P1_PUSHERS = Bit.clear(P1_PUSHERS, right);
					move = Move.ATTACK_PUSHER;
				}
				moves.add(new Move(i, right, board, move, player));
			}

			// Push left ?
			int left2 = i - 14;
			if (Bit.isSet(P2_PUSHEES, left) && Bit.isSet(empty, left2)
					&& isNextRow(i, left) && isSecondRow(i, left2)) {
				Board board = new Board(this);

				board.P2_PUSHEES = Bit.set(P2_PUSHEES, left2);
				board.P2_PUSHEES = Bit.clear(board.P2_PUSHEES, left);

				moves.add(new Move(left, left2, board, Move.MOVE_PUSHEE, player));
			}

			// Push forward ?
			int front2 = i - 16;
			if (Bit.isSet(P2_PUSHEES, front) && Bit.isSet(empty, front2)) {
				Board board = new Board(this);

				board.P2_PUSHEES = Bit.clear(P2_PUSHEES, front);
				board.P2_PUSHEES = Bit.set(board.P2_PUSHEES, front2);

				moves.add(new Move(front, front2, board, Move.MOVE_PUSHEE,
						player));
			}

			// Push right ?
			int right2 = i - 18;
			if (Bit.isSet(P2_PUSHEES, right) && Bit.isSet(empty, right2)
					&& isNextRow(i, right) && isSecondRow(i, right2)) {
				Board board = new Board(this);

				board.P2_PUSHEES = Bit.set(P2_PUSHEES, right2);
				board.P2_PUSHEES = Bit.clear(board.P2_PUSHEES, right);

				moves.add(new Move(right, right2, board, Move.MOVE_PUSHER,
						player));
			}

			// Push attack left ?
			if (Bit.isSet(P2_PUSHEES, left) && Bit.isSet(ennemy, left2)
					&& isNextRow(i, left) && isSecondRow(i, left2)) {
				Board board = new Board(this);

				board.P2_PUSHEES = Bit.set(P2_PUSHEES, left2);
				board.P2_PUSHEES = Bit.clear(board.P2_PUSHEES, left);

				int move = 0;
				// Attacked a pushee ?
				if (Bit.isSet(P1_PUSHEES, left2)) {
					board.P1_PUSHEES = Bit.toggle(P2_PUSHEES, left2);
					move = Move.ATTACK_PUSHEE;
				}// Otherwise it was a pusher
				else {
					board.P1_PUSHERS = Bit.toggle(P2_PUSHERS, left2);
					move = Move.ATTACK_PUSHER;
				}
				moves.add(new Move(left, left2, board, move, player));
			}

			// Push attack right ?
			if (Bit.isSet(P2_PUSHEES, right) && Bit.isSet(ennemy, right2)
					&& isNextRow(i, right) && isSecondRow(i, right2)) {
				Board board = new Board(this);

				board.P2_PUSHEES = Bit.clear(P2_PUSHEES, right);
				board.P2_PUSHEES = Bit.set(board.P2_PUSHEES, right2);

				int move = 0;
				// Attacked a pushee ?
				if (Bit.isSet(P1_PUSHEES, right2)) {
					board.P1_PUSHEES = Bit.clear(P1_PUSHEES, right2);
					move = Move.ATTACK_PUSHEE;
				}// Otherwise it was a pusher
				else {
					board.P1_PUSHERS = Bit.clear(P1_PUSHERS, right2);
					move = Move.ATTACK_PUSHER;
				}
				moves.add(new Move(right, right2, board, move, player));
			}
		}

		if (moves.size() == 0)
			p2_cantMove = true;

		return moves;
	}

	public int getPusherCount(int player) {
		if ((player & 1) == 0)
			return Long.bitCount(P1_PUSHERS);
		else
			return Long.bitCount(P2_PUSHERS);
	}

	public int getPusheeCount(int player) {
		if ((player & 1) == 0)
			return Long.bitCount(P1_PUSHEES);
		else
			return Long.bitCount(P2_PUSHEES);
	}

	/**
	 * 
	 * @param player
	 * @return true if the specified player won the game
	 */
	public boolean playerWon(int player) {
		if ((player & 1) == 0)
			return (getP1() & (255L << 56)) != 0 || p2_cantMove == true;
		else
			return (getP2() & 255L) != 0 || p1_cantMove == true;
	}

	/**
	 * Execute the specified move against the board. If a piece was attacked,
	 * remove it.
	 * 
	 * @param cmd
	 *            A move command as received from the server (Ex.: A2-A3)
	 */
	public void move(String cmd) {
		String[] moves = cmd.trim().split("-");
		String p1 = moves[0].trim();
		String p2 = moves[1].trim();

		System.out.println(cmd);

		int from = coordinatesFrom_A1_TO_H8.indexOf(p1);
		int to = coordinatesFrom_A1_TO_H8.indexOf(p2);
		Move m = new Move(from, to, null, 0, 0);

		// Player one moved ?
		if (Bit.isSet(getP1(), from)) {

			if (m.isOk(this.getP1Moves())) {

				// A pusher was moved ?
				if (Bit.isSet(P1_PUSHERS, from)) {

					P1_PUSHERS = Bit.clear(P1_PUSHERS, from);
					P1_PUSHERS = Bit.set(P1_PUSHERS, to);
				} // A Pushee was moved
				else {
					P1_PUSHEES = Bit.clear(P1_PUSHEES, from);
					P1_PUSHEES = Bit.set(P1_PUSHEES, to);
				}

				// A piece was attacked ?
				if (Bit.isSet(P2_PUSHERS, to)) {
					P2_PUSHERS = Bit.clear(P2_PUSHERS, to);
				} else if (Bit.isSet(P2_PUSHEES, to)) {
					P2_PUSHEES = Bit.clear(P2_PUSHEES, to);
				}

			} else {
				System.out
						.println("Un joueur à joué un coup illégal, la partie doit être arrêter.");
				System.exit(0);
			}

		} // Player 2 moved
		else {

			if (m.isOk(this.getP2Moves())) {

				// A pusher was moved ?
				if (Bit.isSet(P2_PUSHERS, from)) {
					P2_PUSHERS = Bit.clear(P2_PUSHERS, from);
					P2_PUSHERS = Bit.set(P2_PUSHERS, to);
				} // A Pushee was moved
				else {
					P2_PUSHEES = Bit.clear(P2_PUSHEES, from);
					P2_PUSHEES = Bit.set(P2_PUSHEES, to);
				}

				// A piece was attacked ?
				if (Bit.isSet(P1_PUSHERS, to)) {
					P1_PUSHERS = Bit.clear(P1_PUSHERS, to);
				} else if (Bit.isSet(P1_PUSHEES, to)) {
					P1_PUSHEES = Bit.clear(P1_PUSHEES, to);
				}

			} else {
				System.out
						.println("Un joueur à joué un coup illégal, la partie doit être arrêter.");
				System.exit(0);
			}
		}

		printGrid();
	}

	/**
	 * Return true if the Bit b is in the row following a
	 * 
	 * @param Bit
	 * @return
	 */
	private static boolean isNextRow(int a, int b) {
		if (b > a)
			return ((1L << (a + 8)) & (255L << (int) Math.floor(b / 8) * 8)) != 0;
		else
			return ((1L << (a - 8)) & (255L << (int) Math.floor(b / 8) * 8)) != 0;
	}

	/**
	 * Return true if Bit b is 2 row after a
	 * 
	 * @param pieces
	 * @param Bit
	 * @return
	 */
	private static boolean isSecondRow(int a, int b) {
		if (b > a)
			return ((1L << (a + 16)) & (255L << (int) Math.floor(b / 8) * 8)) != 0;
		else
			return ((1L << (a - 16)) & (255L << (int) Math.floor(b / 8) * 8)) != 0;
	}

	private static int[] rows = { 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2,
			2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5,
			5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8,
			8, 8, 8, 8, 8 };

	private static char[] lettersFrom_0_to_63 = { 'A', 'B', 'C', 'D', 'E', 'F',
			'G', 'H', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'A', 'B', 'C',
			'D', 'E', 'F', 'G', 'H', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
			'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'A', 'B', 'C', 'D', 'E',
			'F', 'G', 'H', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'A', 'B',
			'C', 'D', 'E', 'F', 'G', 'H' };

	private static Vector<String> coordinatesFrom_A1_TO_H8;
	private static Vector<Character> letters;

	public static String getCoordinates(int pos) {
		return lettersFrom_0_to_63[pos] + Integer.toString(rows[pos]);
	}

	public static String translatePosition(int from, int to) {
		return getCoordinates(from) + " - " + getCoordinates(to);
	}

	static {
		coordinatesFrom_A1_TO_H8 = new Vector<String>();
		coordinatesFrom_A1_TO_H8.add("A1");
		coordinatesFrom_A1_TO_H8.add("B1");
		coordinatesFrom_A1_TO_H8.add("C1");
		coordinatesFrom_A1_TO_H8.add("D1");
		coordinatesFrom_A1_TO_H8.add("E1");
		coordinatesFrom_A1_TO_H8.add("F1");
		coordinatesFrom_A1_TO_H8.add("G1");
		coordinatesFrom_A1_TO_H8.add("H1");
		coordinatesFrom_A1_TO_H8.add("A2");
		coordinatesFrom_A1_TO_H8.add("B2");
		coordinatesFrom_A1_TO_H8.add("C2");
		coordinatesFrom_A1_TO_H8.add("D2");
		coordinatesFrom_A1_TO_H8.add("E2");
		coordinatesFrom_A1_TO_H8.add("F2");
		coordinatesFrom_A1_TO_H8.add("G2");
		coordinatesFrom_A1_TO_H8.add("H2");
		coordinatesFrom_A1_TO_H8.add("A3");
		coordinatesFrom_A1_TO_H8.add("B3");
		coordinatesFrom_A1_TO_H8.add("C3");
		coordinatesFrom_A1_TO_H8.add("D3");
		coordinatesFrom_A1_TO_H8.add("E3");
		coordinatesFrom_A1_TO_H8.add("F3");
		coordinatesFrom_A1_TO_H8.add("G3");
		coordinatesFrom_A1_TO_H8.add("H3");
		coordinatesFrom_A1_TO_H8.add("A4");
		coordinatesFrom_A1_TO_H8.add("B4");
		coordinatesFrom_A1_TO_H8.add("C4");
		coordinatesFrom_A1_TO_H8.add("D4");
		coordinatesFrom_A1_TO_H8.add("E4");
		coordinatesFrom_A1_TO_H8.add("F4");
		coordinatesFrom_A1_TO_H8.add("G4");
		coordinatesFrom_A1_TO_H8.add("H4");
		coordinatesFrom_A1_TO_H8.add("A5");
		coordinatesFrom_A1_TO_H8.add("B5");
		coordinatesFrom_A1_TO_H8.add("C5");
		coordinatesFrom_A1_TO_H8.add("D5");
		coordinatesFrom_A1_TO_H8.add("E5");
		coordinatesFrom_A1_TO_H8.add("F5");
		coordinatesFrom_A1_TO_H8.add("G5");
		coordinatesFrom_A1_TO_H8.add("H5");
		coordinatesFrom_A1_TO_H8.add("A6");
		coordinatesFrom_A1_TO_H8.add("B6");
		coordinatesFrom_A1_TO_H8.add("C6");
		coordinatesFrom_A1_TO_H8.add("D6");
		coordinatesFrom_A1_TO_H8.add("E6");
		coordinatesFrom_A1_TO_H8.add("F6");
		coordinatesFrom_A1_TO_H8.add("G6");
		coordinatesFrom_A1_TO_H8.add("H6");
		coordinatesFrom_A1_TO_H8.add("A7");
		coordinatesFrom_A1_TO_H8.add("B7");
		coordinatesFrom_A1_TO_H8.add("C7");
		coordinatesFrom_A1_TO_H8.add("D7");
		coordinatesFrom_A1_TO_H8.add("E7");
		coordinatesFrom_A1_TO_H8.add("F7");
		coordinatesFrom_A1_TO_H8.add("G7");
		coordinatesFrom_A1_TO_H8.add("H7");
		coordinatesFrom_A1_TO_H8.add("A8");
		coordinatesFrom_A1_TO_H8.add("B8");
		coordinatesFrom_A1_TO_H8.add("C8");
		coordinatesFrom_A1_TO_H8.add("D8");
		coordinatesFrom_A1_TO_H8.add("E8");
		coordinatesFrom_A1_TO_H8.add("F8");
		coordinatesFrom_A1_TO_H8.add("G8");
		coordinatesFrom_A1_TO_H8.add("H8");

		letters = new Vector<Character>();
		letters.add('A');
		letters.add('B');
		letters.add('C');
		letters.add('D');
		letters.add('E');
		letters.add('F');
		letters.add('G');
		letters.add('H');

		centerMask = 4340410370284600380L;
		p1cornerMask = 195L;
		p2cornerMask = 7025615418697973760L;
		center_squares_Mask = 26214L;
	}

	public void printBoardBits() {
		System.out.println("P1_PUSHERS");
		System.out.println(Long.toBinaryString(P1_PUSHERS));

		System.out.println("P1_PUSHEES");
		System.out.println(Long.toBinaryString(P1_PUSHEES));

		System.out.println("P2_PUSHERS");
		System.out.println(Long.toBinaryString(P2_PUSHERS));

		System.out.println("P2_PUSHEES");
		System.out.println(Long.toBinaryString(P2_PUSHEES));

		System.out.println("Empty");
		System.out.println(Long.toBinaryString(getEmptySquares()));

		System.out.println("NotEmpty");
		System.out.println(Long.toBinaryString(getOccupiedSquares()));

		System.out.println("Occupied by P1");
		System.out.println(Long.toBinaryString(getP1()));

		System.out.println("Occupied by P2");
		System.out.println(Long.toBinaryString(getP2()));
	}

	public void printGrid() {
		System.out.println("");
		int[] grid = new int[64];

		for (int i = 0; i < 64; i++) {
			if (Bit.isSet(P1_PUSHERS, i))
				grid[i] = 2;
		}

		for (int i = 0; i < 64; i++) {
			if (Bit.isSet(P1_PUSHEES, i))
				grid[i] = 1;
		}

		for (int i = 0; i < 64; i++) {
			if (Bit.isSet(P2_PUSHEES, i))
				grid[i] = 3;
		}

		for (int i = 0; i < 64; i++) {
			if (Bit.isSet(P2_PUSHERS, i))
				grid[i] = 4;
		}

		for (int x = 63; x >= 0;) {
			System.out.println("");
			String row = "";

			for (int y = 0; y < 8; y++) {
				int value = grid[x];
				if (value == 2)
					row = "0" + row;
				else if (value == 1)
					row = "o" + row;
				else if (value == 3)
					row = "x" + row;
				else if (value == 4)
					row = "X" + row;
				else
					row = "." + row;
				x--;
			}
			System.out.print(row);
		}

		System.out.println("");
	}

	public long get(int player) {
		if (player == 0)
			return getP1();
		else
			return getP2();
	}

	public int getPlayerCenterPusherCount(int player) {
		if (player == 0) {
			return Long.bitCount(P1_PUSHERS & centerMask);
		} else {
			return Long.bitCount(P2_PUSHERS & centerMask);
		}
	}

	public int getPlayerCornerCount(int player) {
		if (player == 0) {
			return Long.bitCount(P1_PUSHERS & p1cornerMask);
		} else {
			return Long.bitCount(P2_PUSHERS & p2cornerMask);
		}
	}

	public int getPlayerCenterSquarePusherCount(int player) {
		if (player == 0) {
			return Long.bitCount(P1_PUSHERS & center_squares_Mask);
		} else {
			return Long.bitCount(P2_PUSHERS & center_squares_Mask);
		}
	}

	public long getPushers(int player) {
		if (player == 0) {
			return P1_PUSHERS;
		} else {
			return P2_PUSHERS;
		}
	}

	public long getPushees(int player) {
		if (player == 0) {
			return P1_PUSHEES;
		} else {
			return P2_PUSHEES;
		}
	}
}
