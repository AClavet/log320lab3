public class Bit {

	public static long clear(long value, int n) {
		value &= ~(1L << n);
		return value;
	}

	public static long set(long value, int n) {
		value |= (1L << n);
		return value;
	}

	public static long toggle(long value, int n) {
		value ^= (1L << n);
		return value;
	}

	public static boolean isSet(long value, int n) {
		return (value & (1L << n)) != 0;
	}

	public static boolean P2_WON(long value, int n) {
		System.out.println(Long.toBinaryString(value & (255L)));
		return (value & (255L)) != 0;
	}

	public static int count(long x) {
		return Long.bitCount(x);

		/*
		 * int count; for (count = 0; x != 0; count++) x &= x - 1; return count;
		 */
	}

	public static void main(String[] args) {
		Bit bit = new Bit();
		bit.test();
	}

	private void test() {
		long test = Long.MAX_VALUE;
		System.out.println(Long.toBinaryString(Long.MAX_VALUE & 255L));
		System.out.println(Bit.count(test));
		System.out.println(Long.bitCount(test));

		System.out.println(Long.toBinaryString(Board.p2cornerMask
				& Long.MAX_VALUE));

		System.out.println(Long.valueOf("0000000000000000000000000110011001100110",2));
	}

}