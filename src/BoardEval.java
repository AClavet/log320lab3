import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class BoardEval {

	private static BoardEval boardEval = new BoardEval();

	public static BoardEval getInstance() {
		return boardEval;
	}

	private class PersonSortByPerson_ID implements Comparator<Move> {

		@Override
		public int compare(Move m1, Move m2) {
			return m1.score - m2.score;
		}
	}

	int max_player = 0;
	boolean incomplete = false;
	int recursionDepth = 0;
	Move currentMove = null;

	/**
	 * Compute and returns the next best move for the specified player
	 * 
	 * @param board
	 * @param player
	 *            0 = P1, 1 = P2
	 * @return
	 */
	public String getNextMove(Board board, int player) {
		recursionDepth = 1;
		max_player = player;

		long start_ms = System.currentTimeMillis();
		long msElapsed = 0;
		List<Move> moves = board.getMoves(player);
		List<Move> analysed = board.getMoves(player);

		while (recursionDepth < 30) {
			System.out.println("\n" + recursionDepth);
			int moveChecked = 0;

			Collections.sort(moves, new PersonSortByPerson_ID());
			for (Move move : moves) {
				incomplete = false;

				if (move.type != Move.LOST) {
					currentMove = move;

					int val = -negamax(move, recursionDepth, Integer.MIN_VALUE,
							Integer.MAX_VALUE, player ^ 1, start_ms);

					// The move wasn't fully analysed, keep old score.
					if (!incomplete) {
						moveChecked++;
						move.score = recursionDepth % 2 == 0 ? -val : val;
						move.depth = recursionDepth;
						move.type = currentMove.type;

						analysed.add(new Move(move));
						System.out.print(move.score + " ");
					}
				}

				msElapsed = System.currentTimeMillis() - start_ms;
				if (msElapsed >= 4000)
					break;
			}

			if (msElapsed >= 4000) {
				if (moveChecked != moves.size())
					recursionDepth -= 1;
				break;
			} else {
				Move m = checkInevitableChoice(analysed, recursionDepth);
				if (m != null) {
					System.out.println("\n Inevitable {" + player + "} "
							+ m.score);
					return Board.translatePosition(m.from, m.to);
				}
				recursionDepth += 1;
			}
		}

		Move raceMove = checkWinRace(analysed, recursionDepth);

		if (raceMove != null) {
			System.out.println("\n race : {" + player + "} " + raceMove.score);
			return Board.translatePosition(raceMove.from, raceMove.to);
		} else {
			Move bestMove = chooseBestMove(analysed, recursionDepth);

			System.out.println("\n {" + player + "} " + bestMove.score);
			return Board.translatePosition(bestMove.from, bestMove.to);
		}
	}

	private Move checkInevitableChoice(List<Move> moves, int depth) {
		Move move = null;
		int num_vic = 0;

		for (Move m : moves) {
			if (m.depth == depth && m.type != Move.LOST) {
				num_vic++;
				move = m;
			}
		}

		if (num_vic > 0 && num_vic <= 3) {
			int max = Integer.MIN_VALUE;
			for (Move m : moves) {
				if (m.depth == depth && m.type != Move.LOST && m.score > max) {
					move = m;
					max = m.score;
				}
			}
			return move;
		} else
			return null;
	}

	private Move checkWinRace(List<Move> moves, int depth) {
		Move winningMove = null;
		int move_to_win = Integer.MAX_VALUE;
		int move_to_loose = Integer.MAX_VALUE - 1;

		for (Move move : moves) {
			if (move.depth == depth && move.type == Move.WIN) {
				if (move.toWin < move_to_win) {
					move_to_win = move.toWin;
					winningMove = move;
				}
			} else if (move.depth == depth && move.type == Move.LOST) {
				if (move.toLose < move_to_loose) {
					move_to_loose = move.toLose;
				}
			}
		}

		System.out.println("to win :" + move_to_win + " to lose :"
				+ move_to_loose);
		if (move_to_win < move_to_loose)
			return winningMove;
		else
			return null;
	}

	private Move chooseBestMove(List<Move> moves, int depth) {
		Move bestMove = null;
		for (Move move : moves) {
			if (move.depth == depth && move.type != Move.LOST) {
				if (bestMove == null || move.score > bestMove.score)
					bestMove = move;
			}
		}
		while (bestMove == null) {
			depth -= 1;

			for (Move move : moves) {
				if (move.depth == depth && move.type != Move.LOST) {
					if (bestMove == null || move.score > bestMove.score)
						bestMove = move;
				}
			}
			if (bestMove != null)
				break;
			bestMove = checkInevitableChoice(moves, depth);
		}
		return bestMove;
	}

	private int negamax(Move move, int depth, int A, int B, int player,
			long startTimeInMS) {
		Board board = move.board;

		if (System.currentTimeMillis() - startTimeInMS > 4500L) {
			incomplete = true;
			return 0;
		}

		if (depth == 0 || board.playerWon(player)
				|| board.playerWon(player ^ 1)) {
			return evaluate(move, player, depth);
		} else {
			for (Move m : board.getMoves(player)) {
				int val = -negamax(m, depth - 1, -B, -A, player ^ 1,
						startTimeInMS);

				if (val >= B)
					return val;
				if (val >= A)
					A = val;
			}
			return A;
		}
	}

	private int negascout(Move move, int depth, int A, int B, int player) {
		Board board = move.board;

		if (depth == 0 || board.playerWon(player)) {
			return evaluate(move, player, depth);
		} else {
			int b = B;
			for (Move m : board.getMoves(player)) {
				int val = -negascout(m, depth - 1, -b, -A, player ^ 1);

				if (val < A && val < b)
					val = -negascout(m, depth - 1, -B, -A, player ^ 1);
				if (val > A)
					A = val;
				if (A >= B)
					return A;

				b = A + 1;
			}
			return A;
		}
	}

	private int evaluate(Move move, int player, int depth) {
		Board board = move.board;
		int score = 0;

		if (board.playerWon(player)) {
			if (player == max_player) {
				currentMove.type = Move.WIN;
				currentMove.toWin = recursionDepth - depth;
				return 10000 + recursionDepth - depth;
			} else {
				currentMove.type = Move.LOST;
				currentMove.toLose = recursionDepth - depth;
				return -10000 + recursionDepth - depth;
			}
		}

		if (board.playerWon(player ^ 1)) {
			if (player == max_player) {
				currentMove.type = Move.LOST;
				currentMove.toLose = recursionDepth - depth;
				return -10000 + recursionDepth - depth;
			} else {
				currentMove.type = Move.WIN;
				currentMove.toWin = recursionDepth - depth;
				return 10000 + recursionDepth - depth;
			}
		}

		score = 0;

		if (currentMove.type == Move.ATTACK_PUSHER)
			score += 200;
		else if (currentMove.type == Move.ATTACK_PUSHEE)
			score += 50;

		int num_pusher = board.getPusherCount(player) * 50;
		int num_pushee = board.getPusheeCount(player) * 20;

		int num_attack = 0;
		int num_vulnerablePieces = 0;

		int num_corners = board.getPlayerCornerCount(player) * 4;
		int num_center_square = board.getPlayerCenterSquarePusherCount(player) * 2;

		score += num_pusher + num_pushee + num_attack - num_vulnerablePieces
				+ num_corners + num_center_square;

		return (player == max_player) ? score : -score;

	}
}
